// enable coffeescript
require('coffee-script/register');

// server.js (Express 4.0)
var express          = require('express');
var path             = require('path');
var Q                = require('q');
var cluster          = require('cluster');
var os               = require('os');
var figlet           = require('figlet');
var semver           = require('semver');
var requireDirectory = require('require-directory');

// require connect middleware
var morgan         = require('morgan');
var bodyParser     = require('body-parser');
var cookieParser   = require('cookie-parser');
var methodOverride = require('method-override');
var npm            = require('npm');
var path           = require('path');
var _              = require('lodash');

// require app utilities
var util           = require('./util');

/**
 * install npm packages at runtime from app directory
 **/
module.exports.loadModules = function(app,cb) { return Q.Promise(function(resolve,reject){
    // load modules only on master
    if(cluster.worker.id !== 0)
        return resolve(app);

    var opts = app.get("options");
    var resolveCb = typeof cb === 'function' ? cb : function() { resolve(app); };

    // node modules dir
    modulesDir = path.join(opts.app,'node_modules');

    // load package.json from app directory
    var packageJson = null;
    try {
        packageJson = require( path.join(opts.app, "package.json") );
    }
    catch(e) {
        /* we cont care */
    }

    // get module list of required modules
    var moduleList  = [];
    if( packageJson && packageJson.dependencies ) for( var mod in packageJson.dependencies ) {
        var version = packageJson.dependencies[mod];

        // check if we need to install this module
        var shouldInstall = true;
        try {
            var modPackageJson = require( path.join(modulesDir,mod,"package.json") );
            // skip this module if it is already installed
            if( semver.satisfies(modPackageJson.version,version) )
                shouldInstall = false;
        } catch(e) { /* we dont care for this */ }

        // mark module for installation
        if( shouldInstall )
            moduleList.push(mod+"@"+version);
    }

    if( moduleList.length > 0 )
        // install required modules
        npm.load({dependencies:packageJson.dependencies},function(){

            npm.commands.install(opts.app,moduleList,resolveCb);
            npm.on("log",function(log){console.log(log);});
        });
    else {
        // no modules required so start immediatly
        resolveCb();
    }
})};

/** 
 * create and start a new caramba.io server
 * uses home directory for storing server-state like database, temp files...
 * uses app directory for reading app configuration like routes etc.
 * @return express server instance
 **/
module.exports.createServer = function( opts, onReady ) {
    var app     = express(),
        opts    = prepareOptions( typeof opts == 'object' ? opts : null );

    registerSettings( app, opts );

    // list of preloaded functionality
    var preloaded = [];
    preloaded.push( module.exports.loadModules );
    preloaded.push( util.db.startup );

    // execute preloaded list
    var execute = function(preloaded) {
        // execute each preload function after another
        var promise = Q(app);
        preloaded.forEach(function(fn){
            promise = promise.then(fn);
        });

        // handle error in preload chain
        promise.fail( function(e){
            console.log( "App startup failed: " + e );
        });

        return promise;
    };

    var masterFn = function(app) {
        // create a worker for each CPU
        // with a delay of 2 secs between each launch    
        if( opts && opts.cluster ) {
            for( var i = 0; i < opts.cluster; i++ )
                setTimeout( function() { cluster.fork(); }, 2000*i );
        } else {
            // only 1 worker
            cluster.fork();
        }

        // listen for dying workers
        cluster.on( 'exit', function(worker) {
            console.log( "[%s] Worker #{worker.id} died :(", worker.id );
            //cluster.fork()
        });
    };

    var workerFn = function(app) {
        // worker code
        onReady = (typeof opts == 'function' && typeof onReady == 'undefined') ? opts : onReady;

        // worker code
        registerMiddleware( app );

        // auto startup: default is true
        if( opts.start === true ) {
            // execute a default handler or a user-defined onready handler
            app.listen( opts.port, function() {
                return typeof onReady == 'function' ? onReady(app) : defaultReady(app);
            });
        }
    };

    if( opts.nocluster ) {
        preloaded.push(workerFn);
        execute(preloaded);
    }
    else {
        if( cluster.isMaster ) {
            // master startup code
            preloaded.push(masterFn);
            execute(preloaded);
        }
        else {
            // worker startup code
            preloaded.push(workerFn);
            execute(preloaded);
        }
    }

    return app;
}

/**
 * same as createServer() but returns a promise instead of a Express instance
 * @see createServer()
 **/
module.exports.startServer = function( opts ) {
    var deferred = Q.defer(),
        opts     = opts || {start:true};

    module.exports.createServer( opts, function(app) {
        defaultReady(app);
        deferred.resolve( app );
    });

    return deferred.promise;
}

/**
 * get a carambaio configured Express router
 **/
module.exports.defaultRouter = defaultRouter;


var registerSettings = function( app, opts ) {
    app.set( "options", opts );

    if( !app.get("env") )
        app.set( "env", opts.env );
};

var allowXOrigin = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
}

// for html5 mode in AngularJs
// remember to set <base> in your index.html
var fallbackIndexHtml = function(app,opts) {
    // if url does not contain a dot we deliver index.html for html5 mode in angular
    return function(req, res, next) {
        if( req.path.indexOf('.') < 0 )
            res.sendfile( "index.html", { root: opts.public });
        else
            next();
    };
}

var registerMiddleware = function( app, opts ) {
    var opts = opts || app.get("options");

    if( app.get('env') === 'production' ) {
        app.use( bodyParser() );                      // pull information from html in POST
        app.use( methodOverride() );                  // simulate DELETE and PUT
        app.use( cookieParser() );
        app.use( allowXOrigin );
        app.use( defaultRouter(app) );                   // register routes before static files for performance        
        if( opts.public )
            app.use( express.static(opts.public) );       // set the static files location /public/img will be /img for users
        app.use(fallbackIndexHtml(app,opts));
        if( opts.proxy )
            app.use( defaultProxy(app,opts.proxy) );
    }
    else if( app.get('env') === 'development' ) {
        app.use( morgan('dev') );                     // log every request to the console
        app.use( bodyParser() );                      // pull information from html in POST
        app.use( methodOverride() );                  // simulate DELETE and PUT
        app.use( cookieParser() );
        app.use( allowXOrigin );
        app.use( defaultRouter(app) );                   // register routes before static files for performance        
        if( opts.public )
            app.use( express.static(opts.public) );       // set the static files location /public/img will be /img for users
        app.use(fallbackIndexHtml(app,opts));
        if( opts.proxy )
            app.use( defaultProxy(app,opts.proxy) );
    }
};

var defaultProxy = function( app, params ) {
    var proxy     = require('http-proxy').createProxyServer({secure:false});

    var host   = params.host;
    var port   = params.port;
    var path   = params.path;
    var scheme = params.secure ? "https" : "http";

    return function(req, res, next) {
        proxyConf = {target: scheme+"://"+host+":"+port};

        if( !path || path == "" ) {
            proxy.web(req, res, proxyConf);
        }
        else if( path && req.url.match(new RegExp("^\\"+path+"\\/"))) {
            proxyConf.target += path 
            proxy.web(req, res, proxyConf);
        } else {
            next();
        }
    };
};

var defaultRouter = function( app ) {
    var router = express.Router(),
        opts   = app.get('options');

    registerRoutes( router, app, opts );

    return router;
};

var registerRoutes = function( router, app, opts ) {
    var opts   = opts || app.get("options");
    var prefix = null;

    util.registerRoutes( router, app, opts.routes, prefix, /*opts.quiet*/ true );
};

var parseCliArguments = function( opts ) {
    var opts = opts || {};
    var args = {
        string: ['name','cluster','proxy','port','home','app','public','config','models','policies','resolvers','migrations','populations','routes','db'],
        boolean: ["quiet","sqllog","nocluster"],
        default: {
            name:   'caramba.io',
            home:   process.cwd(),
            quiet:  false,
            cluster: 1,
            nocluster: false,
            sqllog: false
        }
    };

    // parse cli parameters
    var argv = require('minimist')(process.argv.slice(2),args);
    args.string.concat(args.boolean).forEach(function(argument) {
        opts[argument] = opts[argument] || argv[argument];
    });

    // load carambaio config from package.json from current directory
    var carambaioConfig = {};
    try {
        var packageJson = require( path.join(process.cwd(), "package.json") );
        if( packageJson && _.isObject(packageJson.carambaio) )
            carambaioConfig = packageJson.carambaio
    }
    catch(e) {
        /* we cont care */
    }

    return _.merge(opts,carambaioConfig);
};

// merge and build options-map with reasonable default parameters
var prepareOptions = function( opts ) {
    opts        = parseCliArguments(opts);

    // core parameter
    opts.env     = process.env.NODE_ENV || 'development';
    opts.start   = typeof opts.start == 'boolean' ? opts.start : true;
    opts.cluster = parseInt(opts.cluster) > 0 ? parseInt(opts.cluster) : os.cpus().length;
    opts.cluster = opts.cluster > os.cpus().length ? os.cpus().length : opts.cluster;
    opts.nocluster = opts.nocluster || false;
    opts.port    = parseInt(opts.port) > 0 ? parseInt(opts.port) : 3000;
    opts.quiet   = opts.quiet  || false;
    opts.sqllog  = opts.sqllog || false;
    opts.worker  = cluster.worker ? cluster.worker.id : 0;
    opts.cwd     = process.cwd();
    opts.version = require(path.join('..','..','package.json')).version;

    // proxy
    if( opts.proxy ) {
        var parts = opts.proxy.split(":");
        opts.proxy = {
            host: parts[0],
            port: parts[1] || 80,
            path: parts[2] || "",
            secure: parts[3] ? true : false
        };
    }

    // directories
    opts.home        = opts.home        || process.cwd();
    opts.home        = path.resolve( opts.home );
    opts.app         = opts.app         || opts.home;
    opts.app         = path.resolve( opts.app );
    opts.public      = opts.public      || path.join(opts.app,"public");
    opts.public      = (opts.public === "off" || opts.public === "false") ? false : path.resolve(opts.public);
    opts.config      = opts.config      || path.join(opts.app,"config");
    opts.config      = path.resolve( opts.config );
    opts.models      = opts.models      || path.join(opts.app,"models");
    opts.models      = path.resolve( opts.models );
    opts.policies    = opts.policies    || path.join(opts.app,"policies");
    opts.policies    = path.resolve( opts.policies );
    opts.resolvers   = opts.resolvers   || path.join(opts.app,"resolvers");
    opts.resolvers   = path.resolve( opts.resolvers );
    opts.migrations  = opts.migrations  || path.join(opts.app,"migrations");
    opts.migrations  = path.resolve( opts.migrations );
    opts.populations = opts.populations || path.join(opts.app,"populations");
    opts.populations = path.resolve( opts.populations );
    opts.routes      = opts.routes      || path.join(opts.app,"routes");
    opts.routes      = path.resolve( opts.routes );
    opts.db          = opts.db          || path.join(opts.home,"db");
    opts.db          = path.resolve( opts.db );

    // resolve config files
    try {
        opts.configuration = requireDirectory(module,opts.config,{visit:function(mod,file){
            try {
                return (typeof mod === "function") ? mod(opts) : mod;
            } catch(e) {
                console.log("Error in config file: " + file + "\n" + e);
            }
        }});
    } catch(e) { opts.configuration = false; };

    return opts;
};


var defaultReady = function(app) {
    var opts = app.get("options");

    if( opts.quiet )
        return;

    var readyMsg = opts.env == 'production' ? "WE ARE LIVE ! ! !" : (opts.env == 'test' ? "test on !" : "ready to develop ;)");

    figlet(opts.name, {font: 'thin'}, function(errTitle, title) {
        figlet( readyMsg, {font: 'thin'}, function(err, msg) {
            if (opts.worker <= 1 && !err && !errTitle) {
                console.log( title );
            }

            // first process shows full output
            if( opts.worker <= 1 ) {
                console.log( "%s server (%s) is running in %s on port %s", 
opts.name, opts.version, opts.env, opts.port );
                console.log( "\t\nusing Home\n\t" + opts.home );
                console.log( "\t\nwith App directory\n\t" + opts.app );
                if( opts.public )
                    console.log( "\t\nserving static files from\n\t" + opts.public );
                else
                    console.log( "\t\nnot serving static files!" );
            }
            // the others less
            if( opts.worker <= 1 && opts.cluster > 1 )
                console.log( "\nspawning %s additional worker processes in about %s seconds...", opts.cluster-1, (opts.cluster-1)*2 );
            // show ready message
            if( opts.worker === opts.cluster )
                console.log( msg );
        });
    });
};
