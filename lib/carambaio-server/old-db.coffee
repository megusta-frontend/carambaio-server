Q           = require 'q'
cluster     = require 'cluster'
Sequelize   = require 'sequelize'
util        = require './old-util'
_           = require 'lodash'
path        = require 'path'
requireDirectory = require 'require-directory'
newOrchestrator  = require('./promise-orchestrator')

cluster.worker = { id: '0' } unless (cluster.worker && cluster.worker.id)

sqlLogger = (app)->
    opts = app.get "options"
    if opts.sqllog
        return (m)-> console.log "[%s] %s", cluster.worker.id, m
    return false

getConnection = (app)->
    opts     = app.get "options"
    config   = opts.configuration
    dbConfig = _.cloneDeep(config?.database?[opts.env] || {
        database: "default",
        username: "root",
        password: "",
        dialect:  "sqlite",
        storage:  ":memory:"
    })

    if !_.isEmpty(dbConfig) and dbConfig.dialect is "sqlite" and dbConfig.storage and dbConfig.storage?.indexOf(":memory:") == -1
        require('mkdirp').sync opts.db
        dbConfig.storage = path.join(opts.db,dbConfig.storage+'.sqlite')
    if !_.isEmpty(dbConfig) and dbConfig.dialect is "sqlite" and !dbConfig?.storage?
        dbConfig.storage = ":memory:"

    # reuse already existing sqlite connection
    # but only if its a sqlite IN-MEMORY database
    # otherwise Sequelize does have its own connection pooling
    existingConnection = app.get "sequelize"
    return existingConnection if existingConnection and dbConfig?.storage?.indexOf(":memory:") > -1

    sequelize = new Sequelize dbConfig.database, dbConfig.username, dbConfig.password,
        dialect: dbConfig.dialect
        logging: sqlLogger(app)
        storage: dbConfig.storage

    return sequelize

models = {}
loadModels = (app,db,t) ->
    opts = app.get "options"

    Models = {}
    try
        Models = requireDirectory(module,opts.models,{exclude:/index/})

    # register models
    definitions = []
    Object.keys(Models).forEach (key)->
        obj = Models[key]
        return if typeof obj isnt "function"
    
        definition      = obj(db,t)
        return unless definition.hasOwnProperty "fields"
        definition.name = definition.name || key

        meta = if definition.hasOwnProperty "meta" then definition.meta else {}
        meta = meta(models) if typeof meta is 'function'

        models[definition.name] = db.define definition.name, definition.fields, meta

        definitions.push definition

    definitions.forEach (definition)->
        return unless (typeof definition["relations"] == 'function')
        definition.relations models[definition.name], models

    return models

createTestdata = (app,sequelize,models) ->
    opts = app.get "options"

    orchestrator = newOrchestrator()

    try requireDirectory module, opts.populations, {exclude: /index|lib|Lib|util|Util/, visit: (populator, joined, filename)->
        return if typeof populator isnt 'function'

        populatorName = path.basename(filename,path.extname(filename))

        # chain of promises
        chain = []
        id    = 0

        # execute populator
        q = populator(app,sequelize,models,Q,chain,orchestrator)

        # if populator worked with chain add each to orchestrator
        if chain.length > 0            
            for cc in [0..chain.length-1]
                id += 1
                orchestrator.add "__#{populatorName}_#{id}", -> cc

        # if populator returned a promsise add it to orchestrator
        if Q.isPromise(q)
            id += 1
            orchestrator.add populatorName, -> q

        # if populator returned a map of tasks add it to orchestrator
        else if _.isObject(q)
            oldKeys = _.keys(q)

            # transform keys to "{populatorName}_{keys}"
            spec = {}
            _.forEach(q, (value,key)->
                spec["#{populatorName}_#{key}"] = value
            )

            # make a group task which depends on every other task in same file
            tasks = _.map oldKeys, (v)->
                return "#{populatorName}_#{v}"

            tasks.push((data)->
                obj = {}

                _.forEach(data, (value,key)->
                    newKey = key.split("_")[1]
                    return if _.indexOf(tasks,key) < 0
                    obj[newKey] = value
                )

                return obj
            )

            orchestrator.add(spec)
            orchestrator.add populatorName, tasks
    }

    # return a Promise with everything resolved
    return orchestrator.resolve()


startup = (app,conn)-> Q.Promise (resolve,reject)->
    opts     = app.get "options"
    config   = opts.configuration
    dbConfig = _.cloneDeep(config?.database?[opts.env] || {})

    # use existing or open new connection
    sequelize = conn || getConnection(app)
    # register models
    loadModels app, sequelize, Sequelize

    # drop all tables for database schema only
    # in development mode and on first worker
    isDev      = app.get('env') == 'development'
    isFirst    = cluster.worker.id < 1
    isInMemory = sequelize.options?.storage?.indexOf(':memory:') > -1
    syncOpts =
        force: isInMemory || (isDev && isFirst)

    # create / synchronize database schema
    sequelize.sync(syncOpts).complete (error)->
        if !!error
            console.log("[%s] Database migration failed: %s", cluster.worker.id, error) if opts.sqllog
            reject error
        else
            console.log("[%s] Database ready!", cluster.worker.id) if opts.sqllog

            app.set "sequelize", sequelize

            # create testdata only with master or in-memory
            if( isInMemory || isFirst )
                createTestdata(app,sequelize,models).then ->
                    console.log("[%s] Database populated!", cluster.worker.id) if opts.sqllog
                    resolve app
            else
                resolve app

# this is no route but a
# pseudo database which holds current state of app (like login sessions)
# for local developing purpose
module.exports.user          = {}

module.exports.models        = models
module.exports.getConnection = getConnection
module.exports.startup       = startup
module.exports.middleware    = (app)->    
    c = getConnection(app)
    m = loadModels app, c, Sequelize

    # return a middleware which makes the app database aware
    database: (req, res, next)->
        req.database = c
        req.models   = m

        next()
