Q     = require 'q'
async = require 'async-q'
_     = require 'lodash'

# resolves a tree of promise dependencies
module.exports = (->
  # task holder
  tasks = {}

  # chainer instance
  return {
    # reset task to empty state
    reset: ->
        tasks = {};

    # add one task or a map of tasks
    add: (name,definition)->
        if _.isObject(name)
            _.merge( tasks, name ) 
        else
            tasks[name] = definition

    # resolve task/promise tree
    resolve: (tt)->
        Q(async.auto(tt || tasks))
  }
)
