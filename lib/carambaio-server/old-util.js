var Q                = require('q');
var cluster          = require('cluster');
var requireDirectory = require('require-directory');
var db               = require('./old-db');
var _                = require('lodash');

module.exports.db    = db;

// if application does not run with cluster support
// make it believe we are in a cluster
if( !cluster.worker || !cluster.worker.id )
    cluster.worker = { id: '0' };

// supported http verbs
var supportedVerbs = ['get','post','put','delete'];
module.exports.supportedVerbs = supportedVerbs;

// require modules from a directory recursivly.
// prepend a prefix if given.
module.exports.loadRoutes = function(dir,prefix) {
    prefix = prefix || "";
    prefix = prefix == "/" ? "" : prefix;

    var routes = [];

    try {
        requireDirectory( module, dir, {visit: function(mod) {
            mod.path = prefix + mod.path;
            routes.push(mod);
        }});
    } catch(e) {
        /* we dont care */
    }

    return routes;
};

// map a list of routes to the express server
module.exports.registerRoutes = function(router, app, routes, prefix, quiet) {
    var opts = app.get("options");

    routes = routes || require(opts.routes);
    prefix = prefix || "";
    quiet  = quiet  || false;
    config = opts.configuration;

    if( typeof routes === 'string' )
        routes = module.exports.loadRoutes(routes,prefix)

    // loop through all routes and register it to the express server
    for( var i in routes ) {
        // evaluate route if function
        var route  = typeof routes[i] === 'function' ? routes[i](app,config) : routes[i];

        // check if route has a path which is required
        if( !route || typeof route.path == 'undefined' || route.path == "undefined" ) {
            console.warn( "[%s] skipping route without a path!", cluster.worker.id );
            continue;
        }

        var path   = prefix + route.path;
        var verbs  = []; // found verbs for this route

        // loop through every supported verb
        // and register a express route for it if available
        for( var i in supportedVerbs ) {
            var verb = supportedVerbs[i];

            // check if the verb has a callback
            if( typeof route[verb] != 'undefined' ) {
                router[verb]( path, buildMiddlewareChain(router,app,route,verb), buildRouteHandler(router,app,route[verb],route,verb) );
                verbs.push(verb);
            }
        }

        if( !quiet )
            console.log( "[%s] registered route: (%s) %s %s", cluster.worker.id, verbs.join("|"), path, route.direct ? '\t(direct)' : '' );
    }
};

// a list of callbacks which may prevent execution or do register some middleware
var buildMiddlewareChain = function( router, app, route, method ) {
    var mw = [];

    mw.push( db.middleware(app).database );
    mw.push( buildPolicyHandler( app, route, method ) );
    mw.push( buildResolver( app, route, method ) );

    return mw;
}

// build security policy handler
var buildPolicyHandler = function( app, route, method ) {
    var opts = app.get("options");

    // does this route have any policies?
    var hasPolicy = typeof route.policy != "undefined" && route.policy != "undefined" && route.policy;
    if( !hasPolicy ) {
        return function(req,res,next) { next() };
    }

    // load policies from policies directory
    var Policies = {};
    var indexPolicy = {};
    try { indexPolicy=require(opts.policies); } catch(e) { /* we dont care */ };
    try { Policies=_.merge(indexPolicy,requireDirectory(module,opts.policies,{exclude:/index/})); } catch(e) { /* we dont care */ };
    var policies = typeof route.policy === 'function' ? route.policy(Policies) : route.policy;

    return function( req, res, next ) {
        var policyMethods = [];

        if( !req.policy ) {
            req.policy = {};
        }

        for( var key in policies ) {
            policyMethods.push( (function(route,key) {
                var policy = policies[key];

                var clearance = null;
                if( typeof policy == 'function' )
                    clearance = Q(policy(req,res));
                else
                    clearance = Q(policy);

                return clearance.then( function(value) {
                    return req.policy[key] = value;
                });
            })(route,key) );
        }

        Q.allSettled(policyMethods).then( function(results) {
            var allowed = false;

            for(var i in results) {
                var result = results[i];

                if( result.state == "fulfilled" && result.value === true ) {
                    allowed = true;
                }
            }

            if( allowed ) {
                next();
            }
            else {
                console.log("[%s] Policy (%s) denied access!", cluster.worker.id, route.path);
                next('route');
            }
        });
    }
}

// a callback-builder for resolving dependencies for a route
var buildResolver = function( app, route, method ) {
    var opts = app.get("options");

    // does this route have any resolver?
    var canResolve = typeof route.resolve != "undefined" && route.resolve != "undefined" && route.resolve;
    if( !canResolve ) {
        return function(req, res, next) { next() };
    }

    // load resolvers from resolvers directory
    var Resolvers = {};
    var indexResolver = {};
    try { indexResolver=require(opts.resolvers); } catch(e) { /* we dont care */ };
    try { Resolvers = _.merge(indexResolver,requireDirectory(module,opts.resolvers,{exclude:/index/})); } catch(e) { /* we dont care */ };

    var resolvers = typeof route.resolve === 'function' ? route.resolve(Resolvers) : route.resolve;

    return function( req, res, next )
    {
        var promises = [];
        // gather all resolvers as promises
        for( var key in resolvers ) {
            // resolve any resolver for this verb
            // and only register verb resolver if it is for the current requested http verb
            if( supportedVerbs.indexOf(key) > -1 && key === method ) {
                // verbResolver is a map
                var verbResolver = resolvers[key];
                for( verbKey in verbResolver ) {
                    promises.push( executeResolver( verbResolver[verbKey], verbKey, app, route, req, res ) );
                }
            }
            // resolve the global route resolver
            else {
                promises.push( executeResolver( resolvers[key], key, app, route, req, res ));
            }
        }

        // no promises so skip to next middleware
        if( promises.length < 1 ) {
            return next();
        }

        // resolve all promises
        // and set values in current request
        Q.allSettled(promises).then( function(results) {
            for(var i in results) {
                var result = results[i];

                if( result.state == "fulfilled" )
                    req.params[result.value.key] = result.value.value;
                else
                    console.log( "[%s] Resolver (%s) could not resolve %s!", cluster.worker.id, result.value.route.path, result.value.key );
            }

            //console.log("[%s] Resolver (%s) resolution done!", cluster.worker.id, result.value.route.path );
            next();
        });
    }    
};

// create isolated context for executing the resolver method
// to prevent asynchronous glitches
// @return a promise
var executeResolver = function(resolver,name,app,route,req,res) {
    var promise  = null;

    if( !Q.isPromise(resolver) ) {
        // if resolver is a function convert the return value into a promise
        if( typeof resolver == 'function' ) {
            // execute resolver method
            // which may return a promise
            promise = resolver( req, res );
            // convert return value to promise if its not already
            promise = Q.isPromise(promise) ? promise : Q(promise);
        }
        // if its just a object convert it into a promise
        else {
            promise = Q(resolver);
        }
    }

    return promise.then( function(value) {
      return { key: name, value: value, route: route };
    });
};

/** retrieve function arguments as list **/
var parseFunctionArgs = function( fn, from ) {
    if( typeof fn !== 'function' )
        return null;
    from = from || 0;

    var s = fn.toString();
    var args = s.substring( s.indexOf('(')+1, s.indexOf(')') ).split(',').map(function(arg){return arg.trim();});
    if( args.length > from )
        return args.slice( from, args.length );

    return [];
};

var resolveDIContext = function(app,req,res,next,annotations) {
    var collect = [];

    annotations.forEach( function(a) {
        var found = undefined;

        if( a === "request" || a === "req" )
            found = req;
        else if( a === "response" || a === "res" )
            found = res;
        else if( a === "next" )
            found = next;
        else if( a === "result" || a === "r" )
            found = buildResultHelper(app,req,res,next);
        else if( a === "ok" )
            found = buildResultHelper(app,req,res,next).ok;
        else if( a === "fail" )
            found = buildResultHelper(app,req,res,next).fail;
        else if( a === "models" || a === "entity" || a === "e" )
            found = req.models;
        else if( req.params[a] )
            found = req.params[a];

        collect.push(found);
    });

    return collect;
};

// a callback-builder method, renders result from callback as json response
var buildRouteHandler = function( router, app, cb, route, method ) {
    var annotations = parseFunctionArgs(cb);

//    if( route.direct ) return function( req, res, next ) {
//        var args = resolveDIContext(app,req,res,next,annotations);
//        return cb.apply(cb,args);
//    }

    return function( req, res, next ) {
        // execute route handler and convert response to promise
        // and render its response as json
        //var deferred = Q.defer();
        var args     = resolveDIContext(app,req,res,next,annotations);
        return Q(cb.apply(cb,args));

//        return result.done( function(result) {
  //          res.status(result.status).json( result );
    //    }, function(result) {
      //      res.status(result.status).json( result );
       // });
    }
};

var buildResultHelper = function(app,req,res,next,deferred) {
    var deferred = deferred || false;

    var renderResponse = function( response, status ) {
        // does not work with .jsonp method
  //      if( app.get('env') == "development" )
    //        response = JSON.stringify(response,null,4);

        return res.status(status).jsonp( response );
//        return res.status(status).type("json").send( response );
    };

    var failFn = function( data, status ) {
        Q(data).then(function(d){
            var status = status || 500;

            var response = {
                failure: true,
                status: status,
                message: "failure",
                code: 10101,
                data: d || {}
            };

            if( deferred ) {
                deferred.reject( response );
                return deferred.promise;
            }

            return renderResponse(response,status);
        });

        return null;
    };

    var okFn = function( data, status ) {
        Q(data).then(function(d){
            var status = status || 200;

            var response = {
                success: true,
                status: status,
                code: 10000,
                message: "success",
                data: d || {}
            };

            if( deferred ) {
                deferred.resolve( response );
                return deferred.promise;
            }

            return renderResponse(response,status);
        }).catch(function(e){
            console.log( "error " + e );
            failFn(e);
        });

        return null;
    };

    var endFn = function( data ) {
        return res.end( data );
    };
    
    var writeStreamFn = function() {
        return res;
    };

    return {
        ok: okFn,

        fail: failFn,

        deny: function( data ) { return failFn(data,403); },

        next: function(e) {
            if(e)
                next(e);
            else
                next();
        },
        
        end: endFn,
        
        writeStream: writeStreamFn
    };
};

// express middleware
// instability: random crashes and delayed request execution
module.exports.middleware = function(app) {
    return {
        instability: function( req, res, next ) {
            // if lagging is enable add lag between 50 and 5000 ms
            var lf = app.get("lagfactor");
            var slowConnectionDelay = app.get("lag") ?
                (50*lf + Math.random() * Math.random() * (1+Math.random()*5000*lf)) :
                0;

            // if crashing is enabled crash randomly
            var cf = app.get("crashfactor");
            var crashFactor = app.get("crash") ? (1+Math.random()*100)*cf : 0;
            if( crashFactor > 85 ) {
                setTimeout( function() { next( new Error("Instability MW simulated crash ("+parseInt(crashFactor)+")! Lag = "+parseInt(slowConnectionDelay)+" ms") ); }, slowConnectionDelay );
            }
            else {
                if( !app.get("quiet") )
                    console.log("[%s] Instability MW: Lag=%s ms, LagFactor: %s, Crash:%s, CrashFactor:%s", cluster.worker.id, parseInt(slowConnectionDelay), parseInt(lf), parseInt(crashFactor), parseInt(cf) );

                setTimeout( next, slowConnectionDelay );
            }
        }
    };
}
