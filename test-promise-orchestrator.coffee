Q            = require 'q'
orchestrator = require './lib/carambaio-server/promise-orchestrator'

# ######################################
# TEST 1
# add tasks one-by-one
# ######################################
# build chainer
chain = orchestrator();
# load online profiles
chain.add "onlineProfiles", -> Q ["a","b","c"]

# load new profiles
chain.add "newProfiles", -> Q ["vf","sdaf","vfd"]

# simulate long database operation
chain.add "allProfiles", ["onlineProfiles","newProfiles",(r)->
  d = Q.defer()

  # simulate long action
  fn = -> d.resolve r.newProfiles.concat(r.onlineProfiles)
  setTimeout fn, 800

  d.promise
]

# and print out result
chain.resolve().then (r)-> console.log r.allProfiles

# ####################################
# TEST 2
# directly resolve task definition map
# ####################################
chain2 = orchestrator()

chain2.resolve({
  get_data: ->
    ### async code to get some data ###
    Q("my_data-asdf")

  make_folder: ->
    ### async code to create a directory to store a file in
        this is run at the same time as getting the data
    ###
    Q("/tmp/data")

  write_file: [
    'get_data'
    'make_folder'
    (results)->
      d = Q.defer()
      ### once there is some data and the directory exists,
          write the data to a file in the directory ###
      fn = ->
        d.resolve("#{results.make_folder}/#{results.get_data}")

      setTimeout fn, 2000

      d.promise
  ]

  email_link: [
    'write_file'
    (results) ->

      ### once the file is written let's email a link to it...
          results.write_file contains the filename returned by write_file.
      ###
      console.log "created: #{results.write_file}"
  ]
}).done()

# ######################################
# TEST 3
# merge independent task definition maps
# ######################################
chain3 = orchestrator()
# map 1
chain3.add
  maestro: [-> Q "maestro"]
  userList: ['maestro',(r)->
    Q [r.maestro].concat(["nina","tina","china"])
  ]

# map 2
chain3.add
  moreUser: ["userList",(r)->
    Q(r.userList.concat(["tom","john","ron"]))
  ]

chain3.resolve().then (r)->
  console.log "All Users: #{r.moreUser}"
